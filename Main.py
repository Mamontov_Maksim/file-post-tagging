import sqlite3

from model.SQLiteDataBase import SQLiteDataBase
from model.FileSeeker import FileSeeker

sqlite = SQLiteDataBase()
tableName = 'Lexicon(word, tag)'


# Обрабатывает текст и вставляет теги
def fileSeekerStart():
    FileSeeker().start()


# Очищает базу данных
def clearAllData():
    sqlite.clearDB()


# Создание базы данных, если еще не создалось
def createDataBaseIfNeed():
    try:
        sqlite.createTable(tableName)
        print('Table was create')
    except sqlite3.OperationalError:
        print('Table was create')


# Показывае дату
def printData():
    print('data: ')
    print(getDataFromDB())


# Получение слов и тегов с базы данных
def getDataFromDB():
    return sqlite.getData()


# Запись слова-тег вручную
def inputDBFromManually():
    # Здесь мы вводим слово
    word = input('Word -> ')
    # Здесь вводим тег для слова
    tag = input('Tag/tags -> ')

    sqlite.insertData(word, tag)


# Метод для записи слово-тег на уровне программы
# Нужно создать файл words.txt для чтения (запись СЛОВО_ТЕГ)
def inputDBFromFile():
    fileName = input('enter file name -> ')
    sqlite.inputDBFromFile(fileName)
    print(fileName)


# Взаимодействие пользователя с консолью
def showConsole():
    print('___________________________________')
    print('|Enter type:                      |')
    print('|0: input data in db from file    |')
    print('|1: input data manually           |')
    print('|2: showing data of db            |')
    print('|3: clear all data, close         |')
    print('|4: start file POST-tagging       |')
    print('|exit: exit from app              |')
    print('|_________________________________|')
    print('')
    while True:
        typeInput = input('please enter type -> ')

        if typeInput == '0':
            inputDBFromFile()
        elif typeInput == '1':
            inputDBFromManually()
        elif typeInput == '2':
            printData()
        elif typeInput == '3':
            clearAllData()
        elif typeInput == '4':
            FileSeeker().start()
        elif typeInput == 'exit':
            break
        else:
            print('choice another variant')


createDataBaseIfNeed()
showConsole()
sqlite.close()
