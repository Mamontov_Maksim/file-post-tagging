from model.SQLiteDataBase import SQLiteDataBase
import re as regular


# __ - privet
class FileSeeker:
    __sqLite = SQLiteDataBase()
    __fileName = ''
    __fileText = ''
    __file = ''

    def start(self):
        self.__inputFile()
        dbData = self.__getDataBaseData()
        self.__fileText = self.__getFileText()

        self.__printTextBeforePOSTagging()
        self.__seek(dbData)
        self.__finish()

    # Ищет в тексте совпадения
    def __seek(self, dbData):
        for data in dbData:
            word = data[0]
            tag = data[1]
            try:
                indexList = [m.start() for m in
                             regular.finditer(word + "[ .,_!?:;&]", self.__fileText, regular.IGNORECASE)]
                if len(indexList) == 0:
                    continue

                # индекс сдвига
                shiftIndex = 0
                for index in indexList:
                    inputIndex = self.__getInputIndex(index + shiftIndex, word)
                    self.__putText(tag, inputIndex)
                    shiftIndex += len("_" + tag)
            except AttributeError:
                print("AttributeError -> " + AttributeError)

            except TypeError:
                print("TypeError -> " + TypeError)

    # Возвращает индекс слова для вставки текста
    def __getInputIndex(self, startIndex, word):
        return startIndex + len(word)

    # Вставляет текст в указанный промежуток и возвращает результат
    def __putText(self, tag, inputIndex):
        self.__fileText = self.__fileText[:inputIndex] + "_" + tag + self.__fileText[inputIndex] + self.__fileText[
                                                                                                   inputIndex + 1:]

    # Загружает текст
    def __getDataBaseData(self):
        return self.__sqLite.getData()

    def __inputFile(self):
        self.__fileName = input('Enter file name for Pos-tagging: ')
        try:
            self.__file = open('../res/text/sample.txt')
        except FileNotFoundError:
            print('File not found error')

    def __printText(self):
        print(self.__fileText)

    def __printTextBeforePOSTagging(self):
        print('Text before POS-tagging')
        self.__printText()

    def __printTextAfterPOSTagging(self):
        print('Text after POS-tagging')
        self.__printText()

    def __getFileText(self):
        return self.__file.read()

    def __closeFile(self):
        self.__file.close()

    def __finish(self):
        self.__closeFile()
        self.__printTextAfterPOSTagging()
