import _sqlite3 as SQLite


class SQLiteDataBase:
    __sqLite = SQLite.connect('data.db')
    __cursor = __sqLite.cursor()

    def createTable(self, tableName):
        self.__cursor.execute('CREATE TABLE ' + tableName)

    def close(self):
        self.__sqLite.close()
        self.__cursor.close()

    def commit(self):
        self.__sqLite.commit()

    def getData(self):
        self.__cursor.execute('SELECT * FROM Lexicon')
        return self.__cursor.fetchall()

    def getTag(self, word):
        self.__cursor.execute('SELECT tag FROM Lexicon WHERE word=' + '"' + word + '"')
        return self.__cursor.fetchall()

    def insertData(self, word, tag):
        self.__cursor.execute('INSERT INTO Lexicon VALUES(?,?)', (word, tag))
        self.commit()

    def clearDB(self):
        self.__cursor.execute('DELETE from Lexicon')
        self.commit()

    def inputDBFromFile(self, fileName):
        try:
            file = open(fileName, encoding='utf-8')
            text = file.read().split('\n')

            for it in text:
                textSplit = it.split('-')
                if len(textSplit) < 2:
                    continue
                word = textSplit[0].replace(' ', '')
                tag = textSplit[1].replace(' ', '')
                self.insertData(word, tag)

            file.close()
        except FileNotFoundError:
            print('File not found ERROR')
        except UnboundLocalError:
            print('Oh, what is it? It is ERRROR mother f****r')
        except IndexError:
            print('What are you doing sucker? - > ' + IndexError)
