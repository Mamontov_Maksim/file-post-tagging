Last season of fashion shows was splendid. We could see models, wearing Chanel, Gucci, MaRnI,
Moschino, Tom Ford, Valentino at the catwalks. There were Russianbabushka headscarves and modest, covered-up folk-costume
dresses next to jewellery brands such as Fiorelli Fiorucci, Flowfold, Safor. A lot of slouch hats and pea jackets; floral
white nightdress and Aran sweater. Gucci logos were everywhere. Among the brands which represent couture we could see Versace,
Ralph & Russo,  Schiapareili, Dior, Elie Saab, Franck Sorbier, Giambattista Valli. All parts of clothes (we are talking about armhole,
armlet, bodice, collar, cuff, epaulette) were way too colourful. Despite of the fact that Pantone Institute chose Almond as a  colour
of the year, we could also sce such colours as Shamrock Caribbean Green, Pine Green, Robin Egg Blue, Aquamarine, Turquoise Blue Among
the star guests we saw (of course) Carine Roitfeld, Anna Wintour, Edward Enninful and Emmanuelle Alt